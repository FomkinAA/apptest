//
//  AuthServiceTest.swift
//  DISPTests
//
//  Created by Anton Fomkin on 28.09.2020.
//  Copyright © 2020 Алексей Михайлов. All rights reserved.
//

import XCTest
import Alamofire
@testable import DISP

class AuthTest: XCTestCase {
    var authService: AuthServiceAbstract!
    var networkService: NetworkServiceMock!
    
    override func setUp() {
        super.setUp()
        networkService = NetworkServiceMock()
        authService = AuthService(baseUrl: URL(string: "test")!,
                                  networkService: networkService)
    }
    
    override func tearDown() {
        networkService = nil
        authService = nil
        super.tearDown()
    }
    
    func testValidateExistingAccountAndReturnSuccessResponse() {
        let userModel = User(id: 777,
                             login: "bill@microsoft.com",
                             name: "Bill",
                             lastname: "Gates")
            
        networkService.result = LoginResponse(result: 1,
                                              user: userModel)
        var resultResponse: LoginResponse?
        
        authService.login(login: "login",
                          password: "password") { [weak self] resultTest in
           
            switch resultTest {
            case .success(let data):
                resultResponse = data
                if resultResponse != nil {
                    XCTAssertNotNil(resultResponse)
                }
            case .failure(let error):
                print(error)
            }
        }
    }
}

