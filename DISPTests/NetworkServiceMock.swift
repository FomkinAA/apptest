//
//  NetworkServiceMock.swift
//  DISPTests
//
//  Created by Anton Fomkin on 28.09.2020.
//  Copyright © 2020 Алексей Михайлов. All rights reserved.
//

import XCTest
import Alamofire
@testable import DISP

typealias Result<Value> = Swift.Result<Value, Error>
    
final class NetworkServiceMock: NetworkServiceAbstract {
    var result: Decodable?
   
    func request<T>(_ request: URLRequestConvertible,
                    completionHandler: @escaping (Result<T>) -> Void) where T : Decodable {
        
        completionHandler(.success(self.result as! T))
    }
}
