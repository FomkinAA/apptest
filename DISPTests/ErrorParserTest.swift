//
//  ErrorParserTest.swift
//  DISPTests
//
//  Created by Anton Fomkin on 28.09.2020.
//  Copyright © 2020 Алексей Михайлов. All rights reserved.
//

import XCTest
@testable import DISP

class ErrorParserTest: XCTestCase {

    var errorParser: ErrorParser!

    override func setUp() {
        super.setUp()
        errorParser = ErrorParser()
    }

    override func tearDown() {
        super.tearDown()
        errorParser = nil
    }

    func testValidateErrorParserAndReturnEqualServerError() {
        let context = DecodingError.Context.init(codingPath: [], debugDescription: "testError")
        let decodeError = DecodingError.dataCorrupted(context)
        let result = errorParser.parse(decodeError)
        XCTAssertEqual(result, AppError.serverError)
    }
}
