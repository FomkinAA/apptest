//
//  NetworkServiceFactoryTest.swift
//  DISPTests
//
//  Created by Anton Fomkin on 28.09.2020.
//  Copyright © 2020 Алексей Михайлов. All rights reserved.
//

import XCTest
@testable import DISP

class NetworkServiceFactoryTest: XCTestCase {
    
    var factory: NetworkServiceFactory!
    
    override func setUp() {
        factory = NetworkServiceFactory()
        super.setUp()
    }
    
    override func tearDown() {
        factory = nil
        super.tearDown()
    }

    func testMakeErrorParserAndReturnSuccessResult() {
        let errorParser = ErrorParser()
        XCTAssertNotNil(errorParser)
    }
    
    func testMakeAuthServiceAndReturnSuccessResult() {
        let authService = factory.makeAuthAuthService()
        XCTAssertNotNil(authService)
    }
}
