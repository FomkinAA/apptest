//
//  DefaultIndentsSetting.swift
//  DISP
//
//  Created by Anton Fomkin on 20.05.2020.
//  Copyright © 2020 Anton Fomkin. All rights reserved.
//

import UIKit

final class DefaultIndentsSetting: IndentsInterface {
 
    let intervalBetweenUIControls: CGFloat = 20.0
    let marginsFromFrame: CGFloat = 20.0
    let heightButton: CGFloat = 45.0
    let cornerRadius: CGFloat = 13.0
    let leftAnchor: CGFloat = 46.0
}
