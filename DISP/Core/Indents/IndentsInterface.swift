//
//  IndentsInterface.swift
//  DISP
//
//  Created by Anton Fomkin on 19.05.2020.
//  Copyright © 2020 Anton Fomkin. All rights reserved.
//

import UIKit

protocol IndentsInterface {
    
    var intervalBetweenUIControls: CGFloat { get }
    var marginsFromFrame: CGFloat { get }
    var heightButton: CGFloat { get }
    var cornerRadius: CGFloat { get }
    var leftAnchor: CGFloat { get }
}
