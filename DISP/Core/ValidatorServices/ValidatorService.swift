//
//  ValidatorService.swift
//  DISP
//
//  Created by Anton Fomkin on 14.08.2020.
//  Copyright © 2020 Алексей Михайлов. All rights reserved.
//

protocol ValidatorService: class {
    
    func validateAccountData(email: String, password: String) -> Bool
    func validatePhoneNumber(_ phoneNumber: String) -> Bool
}
