//
//  TextFieldDataValidator.swift
//  DISP
//
//  Created by Anton Fomkin on 14.08.2020.
//  Copyright © 2020 Алексей Михайлов. All rights reserved.
//

final class TextFieldDataValidator: ValidatorService {
    
    /// Settings for account data
    private let emailRequiredCharacter = "@"
    private let minimumPasswordLength: Int = 8
    
    /// Settings for phone number
    private let phoneNumberRequiredCharacter: Character = "+"
    private let phoneNumberLength: Int = 12
    
    /// Method for verifying account data
    /// - Parameters:
    ///   - email: User e-mail
    ///   - password: User password
    func validateAccountData(email: String, password: String) -> Bool {
        return
            email.contains(emailRequiredCharacter) && password.count >= minimumPasswordLength
    }
    
    /// Method for verifying phone number
    /// - Parameter phoneNumber: User phone number
    func validatePhoneNumber(_ phoneNumber: String) -> Bool {
        return
            phoneNumber.first == phoneNumberRequiredCharacter && phoneNumber.count == phoneNumberLength
    }
}
