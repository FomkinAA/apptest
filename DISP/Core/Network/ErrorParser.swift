//
//  ErrorParser.swift
//  DISP
//
//  Created by Anton Fomkin on 22.04.2020.
//  Copyright © 2020 Anton Fomkin. All rights reserved.
//

import Alamofire

/// Список ошибок сетевого взаимодействия
enum AppError: Error {
    case serverError
    case unknownError
    case networkNotAvailable
}

protocol ErrorParserAbstract {
    
    /// Обработчик ошибок
    /// - Parameter result: Ошибка
    func parse(_ result: Error) -> AppError
    
    /// Обработчик ошибок
    /// - Parameters:
    ///   - request: Параметры запроса
    ///   - response: Параметры ответа сервера
    ///   - data: Полученные данные
    func parse(_ request: URLRequest?, _ response: HTTPURLResponse, _ data: Data?) -> Request.ValidationResult
}

/// Реализция обработчика ошибок сетевого взаимодействия
final class ErrorParser: ErrorParserAbstract {
    
    func parse(_ result: Error) -> AppError {
        if result is DecodingError {
            return .serverError
        }
        
        return .networkNotAvailable
    }
    
    func parse(_ request: URLRequest?, _ response: HTTPURLResponse, _ data: Data?) -> Request.ValidationResult {
        if !(200..<300).contains(response.statusCode) {
            return .failure(AppError.serverError)
        } else if data == nil {
            return .failure(AppError.serverError)
        } else {
            return .success
        }
    }
}
