//
//  NetworkService.swift
//  DISP
//
//  Created by Anton Fomkin on 22.04.2020.
//  Copyright © 2020 Anton Fomkin. All rights reserved.
//

import Alamofire
/*
enum Result<Value> {
    case success(Value)
    case failure(Error)
}
*/
typealias Result<Value> = Swift.Result<Value, Error>

protocol NetworkServiceAbstract {
    /// Выполняет сетевой запрос
    /// - Parameters:
    ///   - request: Параметры запроса
    ///   - completionHandler: Результат запроса
    func request<T: Decodable>(
        _ request: URLRequestConvertible,
        completionHandler: @escaping (Result<T>) -> Void)
}

/// Реализация сетевого слоя
final class NetworkService: NetworkServiceAbstract {
    
    let errorParcer: ErrorParserAbstract
    let sessionManager: SessionManager
    
    init(errorParcer: ErrorParserAbstract,
        sessionManager: SessionManager) {
        self.errorParcer = errorParcer
        self.sessionManager = sessionManager
    }
    
    func request<T: Decodable>(_ request: URLRequestConvertible,
                               completionHandler: @escaping (Result<T>) -> Void) {
        
        let queue = DispatchQueue.global(qos: .utility)
        
        sessionManager
            .request(request)
            .validate(errorParcer.parse)
            .response(queue: queue) { [weak self] resрonse in
                if let error = resрonse.error {
                    let someError = self?.errorParcer.parse(error)
                    guard let networkError = someError else { return }
                    
                    DispatchQueue.main.async {
                        completionHandler(.failure(networkError))
                    }
                } else {
                    do {
                        let value = try JSONDecoder().decode(T.self, from: resрonse.data!)
                        
                        DispatchQueue.main.async {
                            completionHandler(.success(value))
                        }
                    } catch {
                        print(error)
                        
                        DispatchQueue.main.async {
                            completionHandler(.failure(error))
                        }
                    }
                }
        }
    }
}
