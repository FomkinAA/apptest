//
//  RequestRouter.swift
//  DISP
//
//  Created by Anton Fomkin on 22.04.2020.
//  Copyright © 2020 Anton Fomkin. All rights reserved.
//

import Alamofire

/// Варианты кодирования параметров запроса
enum RequestRouterEncoding {
    case url, json
}

/// Описание параметров запроса
protocol RequestRouter: URLRequestConvertible {

    var baseUrl: URL { get }
    var method: HTTPMethod { get }
    var path: String { get }
    var parameters: Parameters? { get }
    var fullUrl: URL { get }
    var encoding: RequestRouterEncoding { get }
}

extension RequestRouter {
    
    var fullUrl: URL {
        return baseUrl.appendingPathComponent(path)
    }

    var encoding: RequestRouterEncoding {
        return .url
    }

    func asURLRequest() throws -> URLRequest {
        let requestTimeout: Double = 3.0
        
        var urlRequest = URLRequest(url: fullUrl)
        urlRequest.httpMethod = method.rawValue
        urlRequest.timeoutInterval = requestTimeout
        
        switch encoding {
        case .url:
            return try URLEncoding.default.encode(urlRequest, with: parameters)
        case .json:
            return try JSONEncoding.default.encode(urlRequest, with: parameters)
        }
    }
}
