//
//  AlerterProvider.swift
//  FitnessTracker
//
//  Created by Anton Fomkin on 08.07.2020.
//  Copyright © 2020 Anton Fomkin. All rights reserved.
//

import UIKit

enum TypeOfAlert {
    case withOk, withOkCancel
}

final class AlerterProvider {
    
    private var presentViewController: UIViewController
    
    private var title: String
    private var message: String
    
    private var typeAlert: TypeOfAlert
    
    private var action: (() -> ())?
    
    init(presentViewController: UIViewController,
         title: String,
         message: String,
         typeAlert: TypeOfAlert,
         action: (() -> ())?) {
        
        self.presentViewController = presentViewController
        self.title = title
        self.message = message
        self.typeAlert = typeAlert
        self.action = action
    }
    
    func makeAlert() -> UIAlertController {
        
        let alerter = UIAlertController(title: title,
                                        message: message,
                                        preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Ok", style: .default) { _ in self.action!() }
        let cancelAction = UIAlertAction(title: "Cancel", style: .default) { _ in }
        
        switch typeAlert {
        case .withOk:
            alerter.addAction(okAction)
        case .withOkCancel:
            alerter.addAction(okAction)
            alerter.addAction(cancelAction)
        }
        
        presentViewController.present(alerter, animated: true, completion: nil)
        
        return alerter
    }
}
