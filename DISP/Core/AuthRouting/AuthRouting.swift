//
//  AuthRouting.swift
//  DISP
//
//  Created by Anton Fomkin on 11.08.2020.
//  Copyright © 2020 Алексей Михайлов. All rights reserved.
//

protocol AuthRouting: class {
  
    func route()
    func routeShowError()
}
