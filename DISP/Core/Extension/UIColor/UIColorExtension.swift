//
//  UIColorExtension.swift
//  DISP
//
//  Created by Anton Fomkin on 11.08.2020.
//  Copyright © 2020 Алексей Михайлов. All rights reserved.
//

import UIKit

extension UIColor {
    
    static let appBackground = UIColor(red: 0.898, green: 0.898, blue: 0.898, alpha: 1)
    static let appFontColor = UIColor(red: 0, green: 0, blue: 0, alpha: 1)
    static let appOrangeColor = UIColor(red: 1, green: 0.72, blue: 0, alpha: 1)
    static let appPlaceholderColor = UIColor(red: 0.435, green: 0.435, blue: 0.4365, alpha: 1)
    
    
    static let appDarkBlue = UIColor(red: 0.57, green: 0.63, blue: 0.78, alpha: 0.5)
    static let appLightGray = UIColor(red: 89.0/255, green: 200.0/255, blue: 228.0/255, alpha: 1)
}
