//
//  NetworkErrorViewController.swift
//  DISP
//
//  Created by Anton Fomkin on 10.05.2020.
//  Copyright © 2020 Anton Fomkin. All rights reserved.
//

import UIKit

final class NetworkErrorViewController: UIViewController {
    
    private(set) lazy var errorMessage: UILabel = {
        let errorMessage = UILabel()
        errorMessage.translatesAutoresizingMaskIntoConstraints = false
        errorMessage.text = "Сеть недоступна. Повторите попытку позже"
        errorMessage.textAlignment = .center
        errorMessage.textColor = .appPlaceholderColor
        errorMessage.font = UIFont(name: "SFUIDisplay-Regular", size: 17.0)
        errorMessage.numberOfLines = 0
        return errorMessage
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBackground
        configureUIControls()
    }
    
    private func configureUIControls() {
        let marginsFromFrame: CGFloat = 20.0
        
        view.addSubview(errorMessage)
        NSLayoutConstraint.activate([
            errorMessage.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            errorMessage.leftAnchor.constraint(equalTo: view.leftAnchor, constant: marginsFromFrame),
            errorMessage.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -marginsFromFrame)
        ])
    }
    
}
