//
//  AuthBuilder.swift
//  DISP
//
//  Created by Anton Fomkin on 06.05.2020.
//  Copyright © 2020 Anton Fomkin. All rights reserved.
//

final class AuthBuilder {
    
    static func build() -> AuthViewInput {
        
        let router = AuthRouter()
        let authService =  NetworkServiceFactory().makeAuthAuthService()
        let validator = TextFieldDataValidator()

        let presenter = AuthPresenter(router: router,
                                      service: authService,
                                      validator: validator)
               
        let indentsSettings = DefaultIndentsSetting()
        
        let authViewController = AuthViewController(presenter: presenter,
                                                    indentsSettings: indentsSettings)
        
        presenter.viewInput = authViewController
        router.presenter = presenter
        
        return authViewController
    }
}
