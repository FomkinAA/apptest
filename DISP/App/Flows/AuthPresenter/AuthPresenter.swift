//
//  AuthPresenter.swift
//  DISP
//
//  Created by Anton Fomkin on 06.05.2020.
//  Copyright © 2020 Anton Fomkin. All rights reserved.
//

import UIKit

final class AuthPresenter {
    
    weak var viewInput: AuthViewInput?
    
    let router: AuthRouting
    let service: AuthServiceAbstract
    let validator: ValidatorService
    
    init(router: AuthRouting,
        service: AuthServiceAbstract,
        validator: ValidatorService) {
        self.router = router
        self.service = service
        self.validator = validator
    }
}

extension AuthPresenter: AuthViewOutput {

    func navigate() { router.route() }   
    func showError() { router.routeShowError() }

    func login(userName: String, password: String) {
        
        viewInput?.buttonsEnabled(false)
     
        service.login(login: userName, password: password) { [weak self] resultLogin in
            
            self?.viewInput?.buttonsEnabled(true)
            
            switch resultLogin {
            case .success( _):
                self?.navigate()
            case .failure(let error):
                print(error)
                self?.showError()
            }
        }
    }
    
    func hideKeyboard() { viewInput?.endEditing() }
    
    func makeErrorAccountAlert() {
        viewInput?.showErrorAccountMessage()
    }
    
    func validateAccoutData(email: String?, password: String?) {
        guard let email = email, let password = password else { return }
        
        let resultValidate = validator.validateAccountData(email: email,
                                                           password: password)
        if !resultValidate {
            makeErrorAccountAlert()
        } else {
            router.route()
        }
    }
}

