//
//  AuthInterface.swift
//  DISP
//
//  Created by Anton Fomkin on 06.05.2020.
//  Copyright © 2020 Anton Fomkin. All rights reserved.
//

import UIKit

protocol AuthViewInput: UIViewController {
    
    var presenter: AuthViewOutput { get }
    var indentsSettings: IndentsInterface { get }
    
    func endEditing()
    func scrollViewTapped()
    func keyboardWasShown(notification: Notification)
    func keyboardWillBeHidden(notification: Notification)
    func buttonsEnabled(_ enabled: Bool)
    
    func showErrorAccountMessage()
}
 
protocol AuthViewOutput: class {
    
    var viewInput: AuthViewInput? { get }
    var router: AuthRouting { get }
    var service: AuthServiceAbstract { get }
    
    func navigate()
    func showError()
    func login(userName: String, password: String)
    func hideKeyboard()
    func makeErrorAccountAlert()
    func validateAccoutData(email: String?, password: String?)
}

