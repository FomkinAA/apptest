//
//  AuthViewController.swift
//  DISP
//
//  Created by Anton Fomkin on 06.05.2020.
//  Copyright © 2020 Anton Fomkin. All rights reserved.
//

import UIKit

final class AuthViewController: UIViewController {
    
    let presenter: AuthViewOutput
    let indentsSettings: IndentsInterface
       
    private(set) lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.autoresizingMask = .flexibleHeight
        
        return scrollView
    }()
       
    private(set) lazy var emailInput: UITextField = {
        let emailInput = UITextField()
        emailInput.translatesAutoresizingMaskIntoConstraints = false
        emailInput.textAlignment = .left
        emailInput.textColor = .appFontColor
        emailInput.placeholder = "E-mail"
        emailInput.font = UIFont(name: "SFUIDisplay-Regular", size: 17.0)
        emailInput.layer.borderWidth = 1.0
        emailInput.backgroundColor = .systemBackground
        emailInput.layer.borderColor = UIColor.systemBackground.cgColor
        emailInput.keyboardType = UIKeyboardType.emailAddress
        emailInput.tag = 30
        emailInput.delegate = self
        
        return emailInput
    }()
    
    private(set) lazy var passwordInput: UITextField = {
        let passwordInput = UITextField()
        passwordInput.translatesAutoresizingMaskIntoConstraints = false
        passwordInput.textAlignment = .left
        passwordInput.textColor = .appFontColor
        passwordInput.placeholder = "Пароль"
        passwordInput.font = UIFont(name: "SFUIDisplay-Regular", size: 17)
        passwordInput.layer.borderWidth = 1.0
        passwordInput.backgroundColor = .systemBackground
        passwordInput.layer.borderColor = UIColor.systemBackground.cgColor
        passwordInput.isSecureTextEntry = true
        passwordInput.tag = 20
        passwordInput.delegate = self
        
        return passwordInput
    }()
    
    private(set) lazy var lineEmail: UIView = {
        let lineEmail = UIView()
        lineEmail.translatesAutoresizingMaskIntoConstraints = false
        lineEmail.backgroundColor = .black
       
        return lineEmail
    }()
    
    private(set) lazy var linePassword: UIView = {
        let linePassword = UIView()
        linePassword.translatesAutoresizingMaskIntoConstraints = false
        linePassword.backgroundColor = .black
       
        return linePassword
    }()
    
    private(set) lazy var recoveryPassword: UIButton = {
        let recoveryPassword = UIButton(type: .system)
        recoveryPassword.translatesAutoresizingMaskIntoConstraints = false
        recoveryPassword.setTitle("Забыли пароль ?", for: .normal)
        recoveryPassword.titleLabel?.font = UIFont(name: "SFUIDisplay-Light", size: 17.0)
        recoveryPassword.setTitleColor(.appOrangeColor, for: .normal)
        recoveryPassword.titleLabel?.textAlignment = .center
        recoveryPassword.layer.cornerRadius = indentsSettings.cornerRadius

        return recoveryPassword
    }()

    private(set) lazy var authSend: UIButton = {
        let authSend = UIButton(type: .system)
        authSend.translatesAutoresizingMaskIntoConstraints = false
        authSend.setTitle("Войти", for: .normal)
        authSend.titleLabel?.font = UIFont(name: "SFUIDisplay-Regular", size: 17.0)
        authSend.setTitleColor(.white, for: .normal)
        authSend.backgroundColor = .appOrangeColor
        authSend.titleLabel?.textAlignment = .center
        authSend.layer.cornerRadius = indentsSettings.cornerRadius

        return authSend
    }()

    init(presenter: AuthViewOutput,
         indentsSettings: IndentsInterface) {
        self.presenter = presenter
        self.indentsSettings = indentsSettings
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBackground
        
    
        let hideKeyboardGesture = UITapGestureRecognizer(target: self, action: #selector(scrollViewTapped))
        scrollView.addGestureRecognizer(hideKeyboardGesture)
        

        configureUIControls()
        configureUIEvents()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        title = "Вход"
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWasShown),
                                               name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillBeHidden(notification:)),
                                               name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self,
                                                  name: UIResponder.keyboardWillShowNotification,
                                                  object: nil)
        NotificationCenter.default.removeObserver(self,
                                                  name: UIResponder.keyboardWillHideNotification,
                                                  object: nil)
    }
    
    override func viewDidLayoutSubviews() {
        guard let navigationBarHeight = navigationController?.navigationBar.bounds.height else { return }
        scrollView.contentSize = CGSize(width: view.frame.width,
                                        height: view.frame.height - navigationBarHeight)
    }
    
    func buttonsEnabled(_ enabled: Bool) {
        authSend.isEnabled = enabled
    }
    
    private func configureUIControls() {
        
        view.addSubview(scrollView)
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0.0),
            scrollView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0.0),
            scrollView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0.0),
            scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0.0),
        ])
        
        let intervalBetweenUIControls = indentsSettings.intervalBetweenUIControls
        let marginsFromFrame = indentsSettings.marginsFromFrame
        let heightButton = indentsSettings.heightButton
        let leftAnchor = indentsSettings.leftAnchor
        
        let safeArea = view.safeAreaLayoutGuide
        guard let centerY = navigationController?.navigationBar.bounds.height else { return }
          
        scrollView.addSubview(emailInput)
        NSLayoutConstraint.activate([
            emailInput.centerYAnchor.constraint(equalTo: scrollView.centerYAnchor, constant: -centerY * 2),
            emailInput.leftAnchor.constraint(equalTo: view.leftAnchor, constant: leftAnchor),
            emailInput.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -leftAnchor),
            emailInput.heightAnchor.constraint(equalToConstant: 34)
        ])
        
        scrollView.addSubview(lineEmail)
         NSLayoutConstraint.activate([
             lineEmail.topAnchor.constraint(equalTo: emailInput.bottomAnchor, constant: 5.0),
             lineEmail.leftAnchor.constraint(equalTo: view.leftAnchor, constant: leftAnchor),
             lineEmail.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -leftAnchor),
             lineEmail.heightAnchor.constraint(equalToConstant: 1)
         ])
        
        scrollView.addSubview(passwordInput)
        NSLayoutConstraint.activate([
            passwordInput.topAnchor.constraint(equalTo: lineEmail.bottomAnchor, constant: intervalBetweenUIControls),
            passwordInput.leftAnchor.constraint(equalTo: view.leftAnchor, constant: leftAnchor),
            passwordInput.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -leftAnchor),
            passwordInput.heightAnchor.constraint(equalToConstant: 34),
        ])
        
        scrollView.addSubview(linePassword)
         NSLayoutConstraint.activate([
             linePassword.topAnchor.constraint(equalTo: passwordInput.bottomAnchor, constant: 5.0),
             linePassword.leftAnchor.constraint(equalTo: view.leftAnchor, constant: leftAnchor),
             linePassword.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -leftAnchor),
             linePassword.heightAnchor.constraint(equalToConstant: 1)
         ])
        
        scrollView.addSubview(recoveryPassword)
         NSLayoutConstraint.activate([
             recoveryPassword.topAnchor.constraint(equalTo: linePassword.bottomAnchor, constant: 9.0),
             recoveryPassword.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -leftAnchor),
             recoveryPassword.heightAnchor.constraint(equalToConstant: 14)
         ])

        scrollView.addSubview(authSend)

        NSLayoutConstraint.activate([
            authSend.bottomAnchor.constraint(equalTo: safeArea.bottomAnchor, constant: -25.0),
            authSend.leftAnchor.constraint(equalTo: view.leftAnchor, constant: leftAnchor),
            authSend.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -leftAnchor),
            authSend.heightAnchor.constraint(equalToConstant: heightButton)
        ])
    }
    
    private func configureUIEvents() {
        authSend.addTarget(self,
                           action: #selector(authSendButtonPressed),
                           for: .touchUpInside)
    }
       
    @objc private func authSendButtonPressed() {
        presenter.validateAccoutData(email: emailInput.text,
                                     password: passwordInput.text)
    }
}

extension AuthViewController: AuthViewInput {
    func endEditing() { scrollView.endEditing(true) }
    
    @objc func scrollViewTapped() { presenter.hideKeyboard() }
    
    @objc func keyboardWasShown(notification: Notification) {
        
        let info = notification.userInfo! as NSDictionary
        let kbSize = (info.value(forKey: UIResponder.keyboardFrameEndUserInfoKey) as! NSValue).cgRectValue.size
        let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: kbSize.height, right: 0.0)
        
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }
    
    @objc func keyboardWillBeHidden(notification: Notification) {
        
        let contentInsets = UIEdgeInsets.zero
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }
    
    func showErrorAccountMessage() {
          
            let _ = AlerterProvider(presentViewController: self,
                                   title: "Ошибка",
                                   message: "Проверьте корректность учетных данных",
                                   typeAlert: .withOk,
                                   action: {})
                .makeAlert()
    }
}

extension AuthViewController: UITextFieldDelegate {
 
    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        return newLength <= textField.tag
    }
}
