//
//  AuthRouter.swift
//  DISP
//
//  Created by Anton Fomkin on 06.05.2020.
//  Copyright © 2020 Anton Fomkin. All rights reserved.
//

final class AuthRouter {
    weak var presenter: AuthViewOutput?
}

extension AuthRouter: AuthRouting {
    
    func route() {
        let stubViewController = StubBuilder.build()
        presenter?.viewInput?.navigationController?.pushViewController(stubViewController,
                                                                       animated: true)
    }
    
    func routeShowError() {
        let networkErrorViewController = NetworkErrorViewController()
        presenter?.viewInput?.navigationController?.navigationBar.isHidden = false
        presenter?.viewInput?.navigationController?.pushViewController(networkErrorViewController,
                                                                       animated: true)
    }
}
