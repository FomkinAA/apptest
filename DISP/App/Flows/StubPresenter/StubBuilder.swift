//
//  StubBuilder.swift
//  DISP
//
//  Created by Anton Fomkin on 18.08.2020.
//  Copyright © 2020 Алексей Михайлов. All rights reserved.
//

final class StubBuilder {
    
    static func build() -> StubViewInput {
        
        let router = StubRouter()
        let authService =  NetworkServiceFactory().makeAuthAuthService()
        let validator = TextFieldDataValidator()

        let presenter = StubPresenter(router: router,
                                      service: authService,
                                      validator: validator)
               
        let indentsSettings = DefaultIndentsSetting()
        
        let stubViewController = StubViewController(presenter: presenter,
                                                    indentsSettings: indentsSettings)
        
        presenter.viewInput = stubViewController
        router.presenter = presenter
        
        return stubViewController
    }
}
