//
//  StubPresenter.swift
//  DISP
//
//  Created by Anton Fomkin on 18.08.2020.
//  Copyright © 2020 Алексей Михайлов. All rights reserved.
//

final class StubPresenter {
    
    weak var viewInput: StubViewInput?
    
    let router: StubRouter
    let service: AuthServiceAbstract
    let validator: ValidatorService
    
    init(router: StubRouter,
        service: AuthServiceAbstract,
        validator: ValidatorService) {
        self.router = router
        self.service = service
        self.validator = validator
    }
}

extension StubPresenter: StubViewOutput {
    
    func stubOneShow() { router.routeStubOne() }
    func stubTwoShow() { router.routeStubTwo() }
    func stubThreeShow() { router.routeStubThree() }
}
