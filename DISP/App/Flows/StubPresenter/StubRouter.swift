//
//  StubRouter.swift
//  DISP
//
//  Created by Anton Fomkin on 18.08.2020.
//  Copyright © 2020 Алексей Михайлов. All rights reserved.
//

final class StubRouter {
    weak var presenter: StubViewOutput?
    
    func routeStubOne() {}
    func routeStubTwo() {}
    func routeStubThree() {}
}
