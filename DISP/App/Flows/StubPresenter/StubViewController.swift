//
//  StubViewController.swift
//  DISP
//
//  Created by Anton Fomkin on 18.08.2020.
//  Copyright © 2020 Алексей Михайлов. All rights reserved.
//

import UIKit

class StubViewController: UIViewController {
  
    let presenter: StubViewOutput
    let indentsSettings: IndentsInterface
 
    init(presenter: StubViewOutput,
         indentsSettings: IndentsInterface) {
        self.presenter = presenter
        self.indentsSettings = indentsSettings
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBackground
        title = "Stub Controller"
    }
}

extension StubViewController: StubViewInput {}
