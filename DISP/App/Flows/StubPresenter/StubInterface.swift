//
//  StubInterface.swift
//  DISP
//
//  Created by Anton Fomkin on 18.08.2020.
//  Copyright © 2020 Алексей Михайлов. All rights reserved.
//

import UIKit

protocol StubViewInput: UIViewController {
    
    var presenter: StubViewOutput { get }
    var indentsSettings: IndentsInterface { get }
}
 
protocol StubViewOutput: class {
    
    var viewInput: StubViewInput? { get }
    var router: StubRouter { get }
    var service: AuthServiceAbstract { get }
    
    func stubOneShow()
    func stubTwoShow()
    func stubThreeShow()
    
}
