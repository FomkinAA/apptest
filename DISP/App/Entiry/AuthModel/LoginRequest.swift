//
//  LoginRequest.swift
//  DISP
//
//  Created by Anton Fomkin on 22.04.2020.
//  Copyright © 2020 Anton Fomkin. All rights reserved.
//

import Alamofire

struct LoginRequest: RequestRouter {

    let baseUrl: URL
    let login: String
    let password: String

    let method: HTTPMethod = .get
    let path: String = "login"
    var parameters: Parameters? {
        return [
            "username": login,
            "password": password
        ]
    }
}
