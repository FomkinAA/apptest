//
//  LoginResponse.swift
//  DISP
//
//  Created by Anton Fomkin on 22.04.2020.
//  Copyright © 2020 Anton Fomkin. All rights reserved.
//

struct LoginResponse: Codable {

    let result: Int
    let user: User
}

struct User: Codable {
 
    let id: Int
    let login: String
    let name: String
    let lastname: String

    private enum CodingKeys: String, CodingKey {
        case id = "id_user"
        case login = "user_login"
        case name = "user_name"
        case lastname = "user_lastname"
    }
}
