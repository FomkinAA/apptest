//
//  AppDelegate.swift
//  disp
//
//  Created by Алексей Михайлов on 18.02.2020.
//  Copyright © 2020 Алексей Михайлов. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var appStartManager: AppStartManager?
    
    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        appStartManager = AppStartManager(window: window)
        appStartManager?.start()
        
        return true
    }
}
