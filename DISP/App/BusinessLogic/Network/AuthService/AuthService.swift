//
//  AuthService.swift
//  DISP
//
//  Created by Anton Fomkin on 11.08.2020.
//  Copyright © 2020 Алексей Михайлов. All rights reserved.
//

import Alamofire

/// Выполнение операций с данными пользователя
final class AuthService: AuthServiceAbstract {
 
    let baseUrl: URL
    let networkService: NetworkServiceAbstract
    
    init(baseUrl: URL,
        networkService: NetworkServiceAbstract) {
        
        self.baseUrl = baseUrl
        self.networkService = networkService
    }
    
    func login(login: String,
               password: String,
               completion: @escaping (Result<LoginResponse>) -> Void) {
        
        let request = LoginRequest(baseUrl: baseUrl,
                                   login: login,
                                   password: password)
        
        networkService.request(request) { (response: Result<LoginResponse>) in
            completion(response)
        }
    }
}
