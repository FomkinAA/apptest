//
//  AuthServiceInterface.swift
//  DISP
//
//  Created by Anton Fomkin on 11.08.2020.
//  Copyright © 2020 Алексей Михайлов. All rights reserved.
//

import Alamofire

protocol AuthServiceAbstract {

    /// Авторизация пользователя
    /// - Parameters:
    ///   - login: Логин
    ///   - password: Пароль
    ///   - completion: Результат запроса
    func login(login: String,
               password: String,
               completion: @escaping (Result<LoginResponse>) -> Void)
}

