//
//  AppStartManager.swift
//  DISP
//
//  Created by Anton Fomkin on 11.08.2020.
//  Copyright © 2020 Алексей Михайлов. All rights reserved.
//

import UIKit

final class AppStartManager {
    
    private var window: UIWindow?
    
    init(window: UIWindow?) {
        self.window = window
    }
    
    func start() {
        
        let rootViewController = AuthBuilder.build()
        
        let navigationViewController = configureNavigationController
        
        rootViewController.navigationController?.navigationBar.isHidden = true
        navigationViewController.viewControllers = [rootViewController]
        
        window?.rootViewController = navigationViewController
        window?.makeKeyAndVisible()
    }
    
    private lazy var configureNavigationController: UINavigationController = {
        let navigationViewController = UINavigationController()

        navigationViewController.navigationBar.isTranslucent = false

        navigationViewController.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        navigationViewController.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black,
                                                                      NSAttributedString.Key.font: UIFont(name: "SFUIDisplay-Regular",
                                                                                                          size: 17.0)!]
        navigationViewController.navigationBar.barTintColor = .systemBackground
        navigationViewController.navigationBar.tintColor = .appOrangeColor
   
        return navigationViewController
    }()
}
