//
//  NetworkServiceFactory.swift
//  DISP
//
//  Created by Anton Fomkin on 11.08.2020.
//  Copyright © 2020 Алексей Михайлов. All rights reserved.
//

import Alamofire

final class NetworkServiceFactory {
    

    let configuration = Configuration()
    let errorParser = ErrorParser()
    
    private(set) lazy var commonSessionManager: SessionManager = {
        let configuration = URLSessionConfiguration.default
        configuration.httpShouldSetCookies = false
        configuration.httpAdditionalHeaders = SessionManager.defaultHTTPHeaders
        let manager = SessionManager(configuration: configuration)
        return manager
    }()

    private(set) lazy var networkService = NetworkService(errorParcer: errorParser,
                                                          sessionManager: commonSessionManager)
    
    func makeAuthAuthService() -> AuthService {
        return AuthService(baseUrl: configuration.baseURL, networkService: networkService)
    }
}
